package uj.jwzp.w4.launchers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uj.jwzp.w4.logic.CSVMovieFinder;
import uj.jwzp.w4.logic.MovieLister;
import uj.jwzp.w4.model.Movie;

@Slf4j
public class SpringMain {

    public static String FILE_NAME = "movies.txt";

    public static void main(String[] args) {
        String fileName = args.length > 0 ? args[0] : FILE_NAME;
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext();
        String finalFileName = fileName;
        configApplicationContext.registerBean(CSVMovieFinder.class, () -> new CSVMovieFinder(finalFileName));
        configApplicationContext.scan("uj.jwzp.w4.logic");
        configApplicationContext.refresh();
        ApplicationContext ctx = configApplicationContext;
        MovieLister lister = (MovieLister) ctx.getBean("movieLister");

        lister.moviesDirectedBy("Hoffman").stream()
                .map(Movie::toString)
                .forEach(SpringMain.log::info);
    }

}
